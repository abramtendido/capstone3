import React, { useEffect, useState } from "react";
import {
    Container,
    Columns,
    Card,
    Button,
    Section,
    Image,
    Dropdown,
    Tag,
    Tabs
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { parseISO, format } from "date-fns";

//queries
import {
    getTeeTimesQuery,
    getBookingsQuery,
    getPendingQuery,
    getUsersQuery,
    getBookingQuery
} from "../queries/queries";

//import
import { deleteBookingMutation } from "../queries/mutations.js";

const Booking = props => {
    console.log(props);

    const [username, setUsername] = useState("");
    const [teeTimeId, setTeeTimeId] = useState("");
    const [status, setStatus] = useState("");
    const [noOfPlayers, setNoOfPlayers] = useState("");

    useEffect(() => {
        console.log("val of date: " + username);
        console.log("val of price: " + teeTimeId);
        console.log("val of time: " + status);
        console.log("val of holes: " + noOfPlayers);
    });

    const deleteBookingHandler = e => {
        console.log(e.target.id);
        let id = e.target.id;

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete the booking!"
        }).then(result => {
            if (result.value) {
                props.deleteBookingMutation({
                    variables: { id: id },
                    refetchQueries: [
                        {
                            query: getBookingsQuery
                        }
                    ]
                });

                Swal.fire("Deleted!", "Booking has been deleted.", "success");
            }
        });
    };

    // console.log(props) "three-fifths" offset="one-fifth";

    const bookingData = props.getBookingsQuery.getBookings
        ? props.getBookingsQuery.getBookings
        : [];

    console.log(bookingData);

    const teeTimeData = props.getTeeTimesQuery.getTeeTimes
        ? props.getTeeTimesQuery.getTeeTimes
        : [];

    const pending = props.getPendingQuery.getPending
        ? props.getPendingQuery.getPending
        : [];
    console.log(pending);

    if (props.getBookingsQuery.loading) {
        let timerInterval;
        Swal.fire({
            title: "Fetching bookings....",
            timer: 1000,
            timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
            onClose: () => {
                clearInterval(timerInterval);
            }
        }).then(result => {
            if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.timer
            ) {
                console.log("I was closed by the timer"); // eslint-disable-line
            }
        });
    }
    const [displayed, setDisplay] = useState("");
    const PendingHandler = () => {
        setDisplay('"display":"none"');
    };

    const [filterBookings, setFilterBookings] = useState("pending");
    const [tagColor, setTagColor] = useState("dark");

    const approvedHandler = event => {
        setFilterBookings("approved");
        setTagColor("success");
    };
    const cancelledHandler = event => {
        setFilterBookings("cancelled");
        setTagColor("danger");
    };

    const pendingHandler = event => {
        setFilterBookings("pending");
        setTagColor("dark");
    };
    return (
        <Container className="column">
            <Columns>
                <Columns.Column size={11} id="bookingcard">
                    <Card id="card3">
                        <Card.Header>
                            <Card.Header.Title
                                className="is-centered "
                                id="title"
                            >
                                Bookings
                            </Card.Header.Title>
                        </Card.Header>
                        <Card.Content id="card-content">
                            {/*sorter*/}
                            <div class="tabs is-toggle is-toggle-rounded is-centered">
                                <ul>
                                    <li>
                                        <Link onClick={pendingHandler}>
                                            Pending
                                        </Link>
                                    </li>
                                    <li>
                                        <a onClick={approvedHandler}>
                                            <span>Approved</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onClick={cancelledHandler}>
                                            <span>Cancelled</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            {/*end of sorter*/}

                            {/*start of all*/}

                            <div
                                id="card-content"
                                className="columns is-multiline has-text-centered"
                            >
                                {bookingData.map(booking => {
                                    let teetimes = booking.teetimes;
                                    let users = booking.users;
                                    if (booking.status === filterBookings) {
                                        return (
                                            <div
                                                id="test"
                                                className="column is-one-third"
                                                key={booking.id}
                                            >
                                                <div class="card">
                                                    <header className="card-header">
                                                        <span
                                                            id="tag"
                                                            class={
                                                                "tag is-" +
                                                                tagColor
                                                            }
                                                        >
                                                            {booking.status}
                                                        </span>
                                                        <p
                                                            id="price"
                                                            className="card-header-title is-centered is-size-5"
                                                        >
                                                            {teetimes
                                                                ? teetimes.time
                                                                : "unassigned"}
                                                        </p>

                                                        <Button
                                                            id={booking.id}
                                                            onClick={
                                                                deleteBookingHandler
                                                            }
                                                            color="dark"
                                                            className="button fas fa-trash-alt btns2 delete "
                                                        ></Button>
                                                    </header>
                                                    <header className="card-header">
                                                        <p
                                                            id="price"
                                                            className="card-header-title is-centered"
                                                        >
                                                            {format(
                                                                parseISO(
                                                                    teetimes.date
                                                                ),
                                                                " E., MMM. d, yyyy"
                                                            )}
                                                        </p>
                                                    </header>

                                                    <div
                                                        id="crd2"
                                                        className="card-content has-text-left"
                                                    >
                                                        <div className="content is-size-7">
                                                            <p id="txt">
                                                                <strong>
                                                                    Holes:
                                                                </strong>
                                                                {"  "}
                                                                {teetimes
                                                                    ? teetimes.holes
                                                                    : "unassigned"}
                                                            </p>
                                                            <p id="txt">
                                                                <strong>
                                                                    Players:
                                                                </strong>
                                                                {"  "}

                                                                {
                                                                    booking.noOfPlayers
                                                                }
                                                            </p>

                                                            <p id="txt">
                                                                <strong>
                                                                    Green Fee:
                                                                </strong>
                                                                {"  "}
                                                                &#8369;
                                                                {teetimes
                                                                    ? teetimes.price
                                                                    : "unassigned"}{" "}
                                                                per player
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div
                                                        id="crd"
                                                        className="card-content has-text-left"
                                                    >
                                                        <div
                                                            id="contain"
                                                            className="content is-size-7"
                                                        >
                                                            <p id="txt">
                                                                <strong>
                                                                    User's
                                                                    Details
                                                                </strong>
                                                            </p>
                                                            <p id="txt">
                                                                <strong>
                                                                    User:
                                                                </strong>
                                                                {"  "}

                                                                {users
                                                                    ? users.firstName
                                                                    : "unassigned"}
                                                                {"  "}

                                                                {users
                                                                    ? users.lastName
                                                                    : "unassigned"}
                                                            </p>

                                                            <p id="txt">
                                                                <strong>
                                                                    Username:
                                                                </strong>
                                                                {"  "}

                                                                {users
                                                                    ? users.username
                                                                    : "unassigned"}
                                                            </p>

                                                            <p id="txt">
                                                                <strong>
                                                                    Email:
                                                                </strong>
                                                                {"  "}

                                                                {users
                                                                    ? users.email
                                                                    : "unassigned"}
                                                            </p>

                                                            <p id="txt">
                                                                <strong>
                                                                    Role:
                                                                </strong>
                                                                {"  "}

                                                                {users
                                                                    ? users.role
                                                                    : "unassigned"}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <footer className="card-footer">
                                                        <div className="btn-group">
                                                            <Link
                                                                to={
                                                                    "booking/update/" +
                                                                    booking.id
                                                                }
                                                            >
                                                                <Button
                                                                    id="edit"
                                                                    className="button is-rounded is-small fas fa-edit is-dark is-outlined"
                                                                ></Button>
                                                            </Link>
                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        );
                                    }
                                })}
                            </div>
                            {/*end of all*/}
                        </Card.Content>
                    </Card>
                </Columns.Column>
            </Columns>
        </Container>
    );
};

export default compose(
    graphql(getTeeTimesQuery, { name: "getTeeTimesQuery" }),
    graphql(getUsersQuery, { name: "getUsersQuery" }),
    graphql(getBookingsQuery, { name: "getBookingsQuery" }),
    graphql(getPendingQuery, { name: "getPendingQuery" }),
    graphql(deleteBookingMutation, { name: "deleteBookingMutation" })
)(Booking);
