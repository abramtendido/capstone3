import { gql } from "apollo-boost";

const getRolesQuery = gql`
	{
		getRoles {
			id
			name
		}
	}
`;

const getStatusesQuery = gql`
	{
		getStatuses {
			id
			name
		}
	}
`;

const getUsersQuery = gql`
	{
		getUsers {
			id
			userName
			firstName
			lastName
			password
			email
			roleID
			role {
				id
				name
			}
		}
	}
`;

const getStudiosQuery = gql`
	{
		getStudios {
			id
			name
			description
			image
			price
		}
	}
`;

const getReservationsQuery = gql`
	{
		getReservations {
			id
			dateStart
			dateEnd
			statusID
			status {
				id
				name
			}
			studioID
			studio {
				id
				name
				price
			}
			userID
			user {
				id
				userName
			}
		}
	}
`;

const getRoleQuery = gql`
	query($id: ID!) {
		getRole(id: $id) {
			id
			name
		}
	}
`;

const getStatusQuery = gql`
	query($id: ID!) {
		getStatus(id: $id) {
			id
			name
		}
	}
`;

const getUserQuery = gql`
	query($id: ID!) {
		getUser(id: $id) {
			id
			userName
			firstName
			lastName
			password
			email
			roleID
		}
	}
`;

const getStudioQuery = gql`
	query($id: ID!) {
		getStudio(id: $id) {
			id
			name
			description
			image
			price
		}
	}
`;

const getReservationQuery = gql`
	query($id: ID!) {
		getReservation(id: $id) {
			id
			dateStart
			dateEnd
			statusID
			studioID
			userID
		}
	}
`;

export {
	getRolesQuery,
	getUsersQuery,
	getRoleQuery,
	getUserQuery,
	getStudiosQuery,
	getStudioQuery,
	getStatusQuery,
	getStatusesQuery,
	getReservationsQuery,
	getReservationQuery
};
