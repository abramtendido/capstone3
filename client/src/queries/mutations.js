import { gql } from "apollo-boost";

// User Mutations
const createUserMutation = gql`
	mutation(
		$userName: String!
		$firstName: String!
		$lastName: String!
		$password: String!
		$email: String!
		$roleID: String!
	) {
		createUser(
			userName: $userName
			firstName: $firstName
			lastName: $lastName
			password: $password
			email: $email
			roleID: $roleID
		) {
			id
			userName
			firstName
			lastName
			password
			email
			roleID
		}
	}
`;

const updateUserMutation = gql`
	mutation(
		$id: ID!
		$userName: String!
		$firstName: String!
		$lastName: String!
		$password: String!
		$roleID: String!
		$email: String!
	) {
		updateUser(
			id: $id
			userName: $userName
			firstName: $firstName
			lastName: $lastName
			password: $password
			email: $email
			roleID: $roleID
		) {
			id
			userName
			firstName
			lastName
			password
			email
			roleID
		}
	}
`;

const deleteUserMutation = gql`
	mutation($id: ID!) {
		deleteUser(id: $id)
	}
`;

const createRoleMutation = gql`
	mutation($name: String!) {
		createRole(name: $name) {
			id
			name
		}
	}
`;

const createStatusMutation = gql`
	mutation($name: String!) {
		createStatus(name: $name) {
			id
			name
		}
	}
`;

const createStudioMutation = gql`
	mutation(
		$name: String!
		$description: String!
		$image: String!
		$price: Float!
	) {
		createStudio(
			name: $name
			description: $description
			image: $image
			price: $price
		) {
			id
			name
			description
			image
			price
		}
	}
`;

const updateStudioMutation = gql`
	mutation(
		$id: ID!
		$name: String!
		$description: String!
		$image: String!
		$price: Float!
	) {
		updateStudio(
			id: $id
			name: $name
			description: $description
			image: $image
			price: $price
		) {
			id
			name
			description
			image
			price
		}
	}
`;

const deleteStudioMutation = gql`
	mutation($id: ID!) {
		deleteStudio(id: $id)
	}
`;

const createReservationMutation = gql`
	mutation(
		$dateStart: Date!
		$dateEnd: Date!
		$studioID: String!
		$statusID: String!
		$userID: String!
	) {
		createReservation(
			dateStart: $dateStart
			dateEnd: $dateEnd
			studioID: $studioID
			statusID: $statusID
			userID: $userID
		) {
			id
			dateStart
			dateEnd
			studioID
			statusID
			userID
		}
	}
`;

const updateReservationMutation = gql`
	mutation($id: ID!, $statusID: String!) {
		updateReservation(id: $id, statusID: $statusID) {
			id
			dateStart
			dateEnd
			studioID
			statusID
			userID
		}
	}
`;

const deleteReservationMutation = gql`
	mutation($id: ID!) {
		deleteReservation(id: $id)
	}
`;

const logInMutation = gql`
	mutation($userName: String!, $password: String!) {
		logInUser(userName: $userName, password: $password) {
			id
			userName
			firstName
			lastName
			password
			email
			roleID
			role {
				name
			}
		}
	}
`;

export {
	createUserMutation,
	createRoleMutation,
	updateUserMutation,
	logInMutation,
	deleteUserMutation,
	createStudioMutation,
	updateStudioMutation,
	deleteStudioMutation,
	createStatusMutation,
	createReservationMutation,
	deleteReservationMutation,
	updateReservationMutation
};
