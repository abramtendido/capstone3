import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Button, Box } from "react-bulma-components";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import "antd/dist/antd.css";

// Components
import NavBar from "./components/NavBar.js";
import Login from "./components/Login.js";
import User from "./components/User.js";
import Studio from "./components/Studio.js";
import Reservation from "./components/Reservation.js";
import CreateReservation from "./components/CreateReservation.js";
import FrontPage from "./components/FrontPage.js";
import NavBarUser from "./components/NavBarUser.js";

//create an instance to all our GraphQL components
// const client = new ApolloClient({ uri: "http://localhost:4000/capstone3" });
const client = new ApolloClient({
  uri: "https://vinystudio-rentals.herokuapp.com/capstone3"
});

function App() {
  const [userName, setUserName] = useState(localStorage.getItem("userName"));
  const [role, setRole] = useState(localStorage.getItem("role"));

  const updateSession = function() {
    setUserName(localStorage.getItem("userName"));
  };

  const Logout = function() {
    localStorage.clear();
    updateSession();
    return <Redirect to="/login" />;
  };

  const loggedUser = function(props) {
    return <Login {...props} updateSession={updateSession} />;
  };

  // console.log(role);
  if (role === "admin") {
    return (
      <ApolloProvider client={client}>
        <BrowserRouter>
          <NavBar userName={userName} />
          <Route exact path="/" component={FrontPage} />
          <Route exact path="/login" render={loggedUser} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/users" component={User} />
          <Route exact path="/studios" component={Studio} />
          <Route exact path="/reservations" component={Reservation} />
          <Route
            path="/reservations/create/:id"
            component={CreateReservation}
          />
        </BrowserRouter>
      </ApolloProvider>
    );
  } else if (role === "user") {
    return (
      <ApolloProvider client={client}>
        <BrowserRouter>
          <NavBarUser userName={userName} />
          <Route exact path="/" component={FrontPage} />
          <Route exact path="/login" render={loggedUser} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/studios" component={Studio} />
          <Route exact path="/reservations" component={Reservation} />
          <Route
            path="/reservations/create/:id"
            component={CreateReservation}
          />
        </BrowserRouter>
      </ApolloProvider>
    );
  } else {
    return (
      <ApolloProvider client={client}>
        <BrowserRouter>
          <NavBarUser userName={userName} />
          <Route exact path="/" component={FrontPage} />
          <Route exact path="/login" render={loggedUser} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/studios" component={Studio} />
          <Route exact path="/reservations" component={Reservation} />
          <Route
            path="/reservations/create/:id"
            component={CreateReservation}
          />
        </BrowserRouter>
      </ApolloProvider>
    );
  }
}

export default App;
