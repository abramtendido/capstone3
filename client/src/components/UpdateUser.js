import React, { useState, useEffect } from "react";
import {
	Section,
	Heading,
	Container,
	Columns,
	Card,
	Button
} from "react-bulma-components";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import Swal from "sweetalert2";

// queries
import { getRolesQuery, getUserQuery } from "../queries/queries.js";
import { updateUserMutation } from "../queries/mutations.js";

const UpdateUser = function(props) {
	console.log(props);

	const [firstName, setFirstName] = useState("");
	const [userName, setUserName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [Role, setRole] = useState("");

	let User = props.getUserQuery.getUser ? props.getUserQuery.getUser : {};

	if (!props.getUserQuery.loading) {
		const setDefault = function() {
			setRole(User.roleID);
			setUserName(User.userName);
			setFirstName(User.firstName);
			setLastName(User.lastName);
			setEmail(User.email);
			setPassword(User.password);
		};

		console.log(User);

		if (Role === "") {
			setDefault();
			// console.log("roleID value after set default" + Role);
		}
	}

	// console.log(User);

	function close() {
		props.close();
	}

	function ChangeHandler(event) {
		if (event.target.id === "fName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lName") {
			setLastName(event.target.value);
		} else if (event.target.id === "emailID") {
			setEmail(event.target.value);
		} else if (event.target.id === "userName") {
			setUserName(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		} else setRole(event.target.value);
	}

	const RoleOptions = function(id) {
		let RoleData = props.getRolesQuery;
		if (RoleData.loading) {
			return <option>Loading ...</option>;
		} else {
			return RoleData.getRoles.map(function(RoleVal) {
				return (
					<option
						value={RoleVal.id}
						key={RoleVal.id}
						selected={RoleVal.id === id}
					>
						{RoleVal.name}
					</option>
				);
			});
		}
	};

	const formSubmitHandler = function(event) {
		event.preventDefault();
		// console.log(props.selectedId);
		let id = props.selectedId;
		let updatedUser = {
			id: id,
			userName: userName,
			firstName: firstName,
			lastName: lastName,
			email: email,
			roleID: Role,
			password: password
		};
		props.updateUserMutation({
			variables: updatedUser
		});
		Swal.fire({
			title: "User Updated",
			showClass: {
				popup: "animated fadeInDown faster"
			},
			hideClass: {
				popup: "animated fadeOutUp faster"
			},
			html:
				'<a href="/users" class="button is-success"> Update Successful </a>',
			showConfirmButton: false
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Heading className="has-text-centered">UpdateUser</Heading>
					<Card>
						<Card.Header>
							<Card.Header.Title>User Details</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<h3>User Name</h3>
								<input
									id="userName"
									type="text"
									onChange={ChangeHandler}
									value={userName}
									className="input"
								/>
								<h3>First Name</h3>
								<input
									id="fName"
									type="text"
									onChange={ChangeHandler}
									value={firstName}
									className="input"
								/>
								<f3>Last Name</f3>
								<input
									id="lName"
									type="text"
									onChange={ChangeHandler}
									value={lastName}
									className="input"
								/>
								<f3>Email</f3>
								<input
									id="emailID"
									type="Email"
									onChange={ChangeHandler}
									value={email}
									className="input"
								/>
								<f3>Role</f3>
								<div className="select is-fullwidth">
									<select onChange={ChangeHandler}>
										{RoleOptions(User.roleID)}
									</select>
								</div>
								<f3>Password</f3>
								<input
									id="password"
									type="Password"
									onChange={ChangeHandler}
									value={password}
									className="input"
								/>
								<br></br>
								<button
									type="submit"
									className="button is-success is-fullwidth"
								>
									Save Changes
								</button>
								<button
									onClick={close}
									type="button"
									className="button is-danger is-fullwidth"
								>
									Cancel
								</button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

// export default UpdateUser;
export default compose(
	graphql(getRolesQuery, { name: "getRolesQuery" }),
	graphql(updateUserMutation, { name: "updateUserMutation" }),
	graphql(getUserQuery, {
		options: props => {
			// console.log(props);
			return {
				variables: {
					id: props.selectedId
				}
			};
		},
		name: "getUserQuery"
	})
)(UpdateUser);
