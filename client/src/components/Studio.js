import React, { useEffect, useState } from "react";
import logo from "./../logo.svg";
import {
	Container,
	Columns,
	Card,
	Button,
	Modal
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import UpdateStudio from "./UpdateStudio";
import { toBase64, nodeServer } from "../function.js";

// queries
import { getStudiosQuery } from "../queries/queries.js";
import {
	createStudioMutation,
	deleteStudioMutation
} from "../queries/mutations.js";

const Studio = function(props) {
	// Hooks
	const [id] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [price, setPrice] = useState("");

	const [updateStudio, setUpdateStudio] = useState(false);
	const [selectedId, setSelectedId] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	console.log(fileRef);

	function ChangeHandler(event) {
		if (event.target.id === "name") {
			setName(event.target.value);
		} else if (event.target.id === "description") {
			setDescription(event.target.value);
		} else if (event.target.id === "image") {
			setImage(event.target.value);
		} else {
			setPrice(event.target.value);
		}
	}

	function showUpdateStudio(event) {
		setUpdateStudio(true);
		setSelectedId(event.target.id);
	}

	function showReserveStudio(event) {}

	function closeUpdateStudio(event) {
		setUpdateStudio(false);
	}

	function deleteStudioHandler(event) {
		let deleteStudio = {
			id: event.target.id
		};

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteStudioMutation({
					variables: deleteStudio,
					refetchQueries: [
						{
							query: getStudiosQuery
						}
					]
				});

				Swal.fire("Deleted!", "Studio has been Deleted", "success");
			}
		});
	}

	function clear() {
		setName("");
		setDescription("");
		setImage("");
		setPrice("");
	}

	const addStudio = function(event) {
		event.preventDefault();
		if (name === "" || description === "" || price === "") {
			Swal.fire({
				title: "Invalid or missing fields!",
				showClass: {
					popup: "animated flipInX"
				},
				hideClass: {
					popup: "animated fadeOutDown faster"
				}
			});
		} else {
			let newStudio = {
				name: name,
				description: description,
				image: imagePath,
				price: parseFloat(price)
			};
			console.log(newStudio.price);
			console.log(typeof newStudio.price);
			props.createStudioMutation({
				variables: newStudio,
				refetchQueries: [
					{
						query: getStudiosQuery
					}
				]
			});
			Swal.fire({
				title: "Studio Added!",
				showClass: {
					popup: "animated flipInY"
				},
				hideClass: {
					popup: "animated fadeOutUp faster"
				}
			});
			clear();
		}
	};

	const data = props.getStudiosQuery;
	const studioData = data.getStudios ? data.getStudios : [];

	const imagePathHandler = function(event) {
		console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			console.log(encodedFile);
			setImagePath(encodedFile);
		});
	};

	return (
		<Container>
			<br></br>
			<Columns className="has-text-centered ">
				<Columns.Column size={3}>
					<Modal show={updateStudio} onClose={closeUpdateStudio}>
						<Modal.Content>
							<UpdateStudio
								selectedId={selectedId}
								close={closeUpdateStudio}
							/>
						</Modal.Content>
					</Modal>
					<Card>
						<Card.Header>Studio</Card.Header>
						<Card.Content>
							<form onSubmit={addStudio}>
								<div className="field ">
									<label className="label">Name</label>
									<input
										id="name"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={name}
									/>
								</div>
								<div className="field ">
									<label className="label">Description</label>
									<input
										id="description"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={description}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="image">
										Image
									</label>
									<div className="control">
										<input
											id="image"
											className="input"
											type="file"
											accept="image/png"
											onChange={imagePathHandler}
											ref={fileRef}
										/>
									</div>
								</div>
								<div className="field">
									<label className="label">Price</label>
									<input
										id="price"
										className="input"
										type="number"
										onChange={ChangeHandler}
										value={price}
									/>
								</div>

								<Button
									onClick={addStudio}
									type="submit"
									color="success"
									fullwidth
									rounded
								>
									Add Studio
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header.Title>All Studios</Card.Header.Title>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<th>Name</th>
										<th>Description</th>
										<th>Image</th>
										<th>Price</th>
										<th>Action</th>
									</thead>
									<tbody>
										{studioData.map(function(studio) {
											return (
												<tr>
													<td>{studio.name}</td>
													<td>
														{studio.description}
													</td>
													<td>
														<img
															src={
																nodeServer() +
																studio.image
																// studio.image
															}
														/>
													</td>
													<td>
														{studio.price} per day
													</td>

													<td>
														<Link
															to={
																"/reservations/create/" +
																studio.id
															}
														>
															<Button
																id={studio.id}
																color="success"
																fullwidth
															>
																Reserve
															</Button>
														</Link>
														<Button
															onClick={
																showUpdateStudio
															}
															id={studio.id}
															color="link"
															fullwidth
														>
															Update
														</Button>

														<Button
															id={studio.id}
															onClick={
																deleteStudioHandler
															}
															type="submit"
															color="danger"
															fullwidth
														>
															Delete
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getStudiosQuery, { name: "getStudiosQuery" }),
	graphql(createStudioMutation, { name: "createStudioMutation" }),
	graphql(deleteStudioMutation, { name: "deleteStudioMutation" })
)(Studio);
