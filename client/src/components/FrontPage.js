import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Carousel } from "antd";
import "../App.css";

const FrontPage = function(props) {
  return (
    <Carousel autoplay>
      <div>
        <img className="mx-auto img-fluid" src="./images/studio1.png" />
      </div>
      <div>
        <img className="mx-auto img-fluid" src="./images/studio2.png" />
      </div>
      <div>
        <img className="mx-auto img-fluid" src="./images/studio3.png" />
      </div>
      <div>
        <img className="mx-auto img-fluid" src="./images/studio4.png" />
      </div>
    </Carousel>
  );
};

export default FrontPage;
