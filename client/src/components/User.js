import React, { useEffect, useState } from "react";
import logo from "./../logo.svg";
import {
	Container,
	Columns,
	Card,
	Button,
	Modal
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import UpdateUser from "./UpdateUser";

// queries
import { getUsersQuery, getRolesQuery } from "../queries/queries.js";
import {
	createUserMutation,
	deleteUserMutation
} from "../queries/mutations.js";

const User = function(props) {
	// Hooks
	const [id] = useState("");
	const [userName, setUserName] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [password, setPassword] = useState("");
	const [email, setEmail] = useState("");
	const [role, setRole] = useState("");

	const [updateUser, setUpdateUser] = useState(false);
	const [selectedId, setSelectedId] = useState("");

	function ChangeHandler(event) {
		if (event.target.id === "fName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lName") {
			setLastName(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "userName") {
			setUserName(event.target.value);
		} else if (event.target.id === "email") {
			setEmail(event.target.value);
		} else setRole(event.target.value);
	}

	function showUpdateUser(event) {
		setUpdateUser(true);
		setSelectedId(event.target.id);
	}

	function closeUpdateUser(event) {
		setUpdateUser(false);
	}

	function deleteUserHandler(event) {
		console.log(event.target.id);
		let deleteUser = {
			id: event.target.id
		};

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteUserMutation({
					variables: deleteUser,
					refetchQueries: [
						{
							query: getUsersQuery
						}
					]
				});

				Swal.fire("Deleted!", "User has been Deleted", "success");
			}
		});
	}

	function clear() {
		setFirstName("");
		setLastName("");
		setPassword("");
		setUserName("");
		setEmail("");
	}

	const addUser = function(event) {
		event.preventDefault();
		console.log("ROLE: " + role);

		let emailTest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
			email
		);
		console.log(emailTest);

		if (
			userName === "" ||
			password === "" ||
			email === "" ||
			firstName === "" ||
			lastName === "" ||
			role === "" ||
			!emailTest
		) {
			Swal.fire({
				title: "Invalid or missing fields!",
				showClass: {
					popup: "animated flipInX"
				},
				hideClass: {
					popup: "animated fadeOutDown faster"
				}
			});
		} else {
			let newUser = {
				userName: userName,
				firstName: firstName,
				lastName: lastName,
				password: password,
				email: email,
				roleID: role
			};

			props.createUserMutation({
				variables: newUser,
				refetchQueries: [
					{
						query: getUsersQuery
					}
				]
			});
			Swal.fire({
				title: "User Added!",
				showClass: {
					popup: "animated flipInY"
				},
				hideClass: {
					popup: "animated fadeOutUp faster"
				}
			});
			clear();
		}
	};

	const data = props.getUsersQuery;
	const userData = data.getUsers ? data.getUsers : [];

	const roleOptions = function() {
		let roleData = props.getRolesQuery;

		if (roleData.loading) {
			return <option>Loading.... </option>;
		} else {
			return roleData.getRoles.map(function(role) {
				return (
					<option value={role.id} key={role.id}>
						{role.name}
					</option>
				);
			});
		}
	};

	return (
		<Container>
			<br></br>
			<Columns className="has-text-centered ">
				<Columns.Column size={3}>
					<Modal show={updateUser} onClose={closeUpdateUser}>
						<Modal.Content>
							<UpdateUser
								selectedId={selectedId}
								close={closeUpdateUser}
							/>
						</Modal.Content>
					</Modal>
					<Card>
						<Card.Header>User</Card.Header>
						<Card.Content>
							<form onSubmit={addUser}>
								<div className="field ">
									<label className="label" htmlFor="fname">
										First Name
									</label>
									<input
										id="fName"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={firstName}
									/>
								</div>
								<div className="field ">
									<label className="label" htmlFor="fname">
										UserName
									</label>
									<input
										id="userName"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={userName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="lname">
										Last Name
									</label>
									<input
										id="lName"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={lastName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="Password"
										onChange={ChangeHandler}
										value={password}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="email">
										Email
									</label>
									<input
										id="email"
										className="input"
										type="email"
										onChange={ChangeHandler}
										value={email}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="roleName">
										Role
									</label>
									<div className="select">
										<select onChange={ChangeHandler}>
											<option disabled selected>
												Select Role
											</option>
											{roleOptions()}
										</select>
									</div>
								</div>

								<Button
									onClick={addUser}
									type="submit"
									color="success"
									fullwidth
									rounded
								>
									Add User
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header.Title>All Users</Card.Header.Title>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<th>User Name</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Role Name</th>
										<th>Action</th>
									</thead>
									<tbody>
										{userData.map(function(user) {
											let role = user.role;

											return (
												<tr>
													<td>{user.userName}</td>
													<td>{user.firstName}</td>
													<td>{user.lastName}</td>
													<td>{user.email}</td>
													<td>
														{role
															? role.name
															: "unassigned"}
													</td>
													<td>
														<Button
															onClick={
																showUpdateUser
															}
															id={user.id}
															color="link"
															fullwidth
														>
															Update
														</Button>

														<Button
															id={user.id}
															onClick={
																deleteUserHandler
															}
															type="submit"
															color="danger"
															fullwidth
														>
															Delete
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(getRolesQuery, { name: "getRolesQuery" }),
	graphql(createUserMutation, { name: "createUserMutation" }),
	graphql(deleteUserMutation, { name: "deleteUserMutation" })
)(User);
