import React, { useEffect, useState } from "react";
import logo from "./../logo.svg";
import {
    Container,
    Columns,
    Card,
    Button,
    Modal
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

// queries
import {
    getReservationsQuery,
    getStatusesQuery,
    getStudiosQuery,
    getUsersQuery
} from "../queries/queries.js";
import {
    deleteReservationMutation,
    updateReservationMutation
} from "../queries/mutations.js";

const Reservation = function(props) {
    const [userID, setUserID] = useState("");
    const [dateStart, setDateStart] = useState("");
    const [dateEnd, setDateEnd] = useState("");
    const [studioID, setStudioID] = useState("");
    const [statusID, setStatusID] = useState("");

    const reservationData = props.getReservationsQuery.getReservations
        ? props.getReservationsQuery.getReservations
        : [];

    const studioData = props.getStudiosQuery.getStudios
        ? props.getStudiosQuery.getStudios
        : [];

    const statusData = props.getStatusesQuery.getStatuses
        ? props.getStatusesQuery.getStatuses
        : [];

    const [filterReservations, setFilterReservations] = useState("pending");
    const [tagColor, setTagColor] = useState("dark");

    const userLocal = localStorage.getItem("userName");
    const role = localStorage.getItem("role");

    function pending(event) {
        setFilterReservations("pending");
        setTagColor("warning");
        console.log(filterReservations);
    }

    function cancel(event) {
        setFilterReservations("cancelled");
        setTagColor("danger");
        console.log(filterReservations);
    }

    function completed(event) {
        setFilterReservations("approved");
        setTagColor("info");
        console.log(filterReservations);
    }

    // const statusApproved = function(event) {
    //     statusData.map(function(status) {
    //         if (status.name === "approved") {
    //             // statusIDTemp = status;
    //             setStatusID(status.id);
    //             console.log(status.name);
    //             console.log(status.id);
    //             console.log(statusID);
    //         }
    //     });
    //     console.log(statusID);
    // };

    const approveReservationHandler = function(event) {
        // event.preventDefault();
        let id = event.target.id;
        // statusData.map(function(status) {
        //     if (status.name === "approved") {
        //         statusIDTemp = status.id;
        //         setStatusID(statusIDTemp);

        //         console.log(status.name);
        //         console.log(status.id);
        //         console.log(statusIDTemp);
        // console.log(id);
        // console.log(statusID);
        //     }
        // });
        // setStatusID("5deedf5b7c9f6813807abe7a");

        let updatedReservation = {
            id: id,
            statusID: "5deedf5b7c9f6813807abe7a"
        };

        props.updateReservationMutation({
            variables: updatedReservation
        });
        Swal.fire({
            title: "Reservation Approved",
            showClass: {
                popup: "animated fadeInDown faster"
            },
            hideClass: {
                popup: "animated fadeOutUp faster"
            },
            html:
                '<a href="/reservations" class="button is-success"> Approved </a>',
            showConfirmButton: false
        });
    };

    function cancelReservationHandler(event) {
        let id = event.target.id;
        statusData.map(function(status) {
            if (status.name === "cancelled") {
                console.log(status.name);
                setStatusID(status.id);
            }
        });
        // setStatusID("5deedf617c9f6813807abe7b");
        let updatedReservation = {
            id: id,
            // statusID: statusID
            statusID: "5deedf617c9f6813807abe7b"
        };
        props.updateReservationMutation({
            variables: updatedReservation
        });
        Swal.fire({
            title: "Reservation Cancelled",
            showClass: {
                popup: "animated fadeInDown faster"
            },
            hideClass: {
                popup: "animated fadeOutUp faster"
            },
            html:
                '<a href="/reservations" class="button is-success"> Cancelled </a>',
            showConfirmButton: false
        });
    }

    function deleteReservationHandler(event) {
        event.preventDefault();
        console.log(event.target.id);
        let deleteReservation = {
            id: event.target.id
        };

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then(result => {
            if (result.value) {
                props.deleteReservationMutation({
                    variables: deleteReservation,
                    refetchQueries: [
                        {
                            query: getReservationsQuery
                        }
                    ]
                });

                Swal.fire(
                    "Deleted!",
                    "Reservation has been Deleted",
                    "success"
                );
            }
        });
    }

    return (
        <Container className="column">
            <Columns>
                <Columns.Column size={12} id="reservationcard">
                    <Card id="card3">
                        <Card.Header>
                            <Card.Header.Title
                                className="is-centered "
                                id="title"
                            >
                                Reservations
                            </Card.Header.Title>
                        </Card.Header>
                        <Card.Content id="card-content">
                            {/*sorter*/}
                            <div class="tabs is-toggle is-toggle-rounded is-centered">
                                <ul>
                                    <li>
                                        <Link onClick={pending}>Pending</Link>
                                    </li>
                                    <li>
                                        <a onClick={completed}>
                                            <span>Approved</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onClick={cancel}>
                                            <span>Cancelled</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            {/*end of sorter*/}

                            {/*start of all*/}

                            <div
                                id="card-content"
                                className="columns is-multiline has-text-centered"
                            >
                                {reservationData.map(reservation => {
                                    let studio = reservation.studio;
                                    let user = reservation.user;
                                    let statusVal = reservation.status.name
                                        ? reservation.status.name
                                        : "pending";
                                    // console.log(user);
                                    // console.log(reservation.id);
                                    // console.log(reservation.status.name);
                                    // console.log(filterReservations);
                                    // console.log("sadasd");
                                    // console.log(statusVal);
                                    console.log(user.userName);
                                    console.log(user);

                                    // statusVal === filterReservations
                                    //     ? console.log("equal")
                                    //     : console.log("not2");

                                    if (
                                        statusVal === filterReservations &&
                                        user.userName === userLocal
                                    ) {
                                        return (
                                            <div
                                                id="test"
                                                className="column is-one-third"
                                                key={reservation.id}
                                            >
                                                <div class="card">
                                                    <header className="card-header">
                                                        <span
                                                            id="tag"
                                                            class={
                                                                "tag is-" +
                                                                tagColor
                                                            }
                                                        >
                                                            {
                                                                reservation
                                                                    .status.name
                                                            }
                                                        </span>
                                                        <p
                                                            id="price"
                                                            className="card-header-title is-centered is-size-5"
                                                        >
                                                            {
                                                                reservation.dateStart
                                                            }{" "}
                                                            to{" "}
                                                            {
                                                                reservation.dateEnd
                                                            }
                                                        </p>
                                                    </header>
                                                    <div
                                                        id="crd2"
                                                        className="card-content has-text-left"
                                                    >
                                                        <div className="content is-size-7">
                                                            <p id="txt">
                                                                <strong>
                                                                    Studio:
                                                                </strong>
                                                                {studio.name
                                                                    ? studio.name
                                                                    : "unassigned"}
                                                            </p>
                                                            <p id="txt">
                                                                <strong>
                                                                    User:
                                                                </strong>
                                                                {"  "}

                                                                {user.userName}
                                                            </p>

                                                            <p id="txt">
                                                                <strong>
                                                                    Price:
                                                                </strong>
                                                                {"  "}
                                                                &#8369;
                                                                {studio.price
                                                                    ? studio.price
                                                                    : "unassigned"}{" "}
                                                                per day
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <footer className="card-footer">
                                                        <div className="btn-group">
                                                            <button
                                                                className="button is-success fas fa-check"
                                                                id={
                                                                    reservation.id
                                                                }
                                                                onClick={
                                                                    approveReservationHandler
                                                                }
                                                                type="button"
                                                            >
                                                                Approve
                                                            </button>

                                                            <button
                                                                className="mx-2 button is-danger is-outlined fas fa-times"
                                                                id={
                                                                    reservation.id
                                                                }
                                                                onClick={
                                                                    cancelReservationHandler
                                                                }
                                                                type="button"
                                                            >
                                                                Cancel
                                                            </button>

                                                            <button
                                                                id={
                                                                    reservation.id
                                                                }
                                                                className="fas fa-times button is-dark is-outlined "
                                                                onClick={
                                                                    deleteReservationHandler
                                                                }
                                                            >
                                                                Delete
                                                            </button>
                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        );
                                    } else if (
                                        statusVal === filterReservations &&
                                        role === "admin"
                                    ) {
                                        return (
                                            <div>
                                                <h1>ADMIN</h1>
                                            </div>
                                        );
                                    }
                                })}
                            </div>
                            {/*end of all*/}
                        </Card.Content>
                    </Card>
                </Columns.Column>
            </Columns>
        </Container>
    );
};
export default compose(
    graphql(getReservationsQuery, { name: "getReservationsQuery" }),
    graphql(deleteReservationMutation, { name: "deleteReservationMutation" }),
    graphql(updateReservationMutation, { name: "updateReservationMutation" }),
    graphql(getUsersQuery, { name: "getUsersQuery" }),
    graphql(getStudiosQuery, { name: "getStudiosQuery" }),
    graphql(getStatusesQuery, { name: "getStatusesQuery" })
)(Reservation);
