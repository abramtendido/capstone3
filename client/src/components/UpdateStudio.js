import React, { useState, useEffect } from "react";
import {
	Section,
	Heading,
	Container,
	Columns,
	Card,
	Button
} from "react-bulma-components";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import Swal from "sweetalert2";

// queries
import { getStudioQuery } from "../queries/queries.js";
import { updateStudioMutation } from "../queries/mutations.js";

const UpdateStudio = function(props) {
	console.log(props);

	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [price, setPrice] = useState("");

	let Studio = props.getStudioQuery.getStudio
		? props.getStudioQuery.getStudio
		: {};

	useEffect(function() {
		console.log(name);
	});

	if (!props.getStudioQuery.loading) {
		const setDefault = function() {
			setId(Studio.id);
			setName(Studio.name);
			setDescription(Studio.description);
			setImage(Studio.image);
			setPrice(Studio.price);
		};

		console.log(Studio);
		if (id === "") {
			setDefault();
			console.log("id value after set default" + Studio.id);
		}
	}

	function close() {
		props.close();
	}

	function ChangeHandler(event) {
		if (event.target.id === "name") {
			setName(event.target.value);
		} else if (event.target.id === "description") {
			setDescription(event.target.value);
		} else if (event.target.id === "image") {
			setImage(event.target.value);
		} else {
			setPrice(event.target.value);
		}
	}

	const formSubmitHandler = function(event) {
		event.preventDefault();
		let id = props.selectedId;
		let updatedStudio = {
			id: id,
			name: name,
			description: description,
			image: image,
			price: parseFloat(price)
		};
		props.updateStudioMutation({
			variables: updatedStudio
		});
		Swal.fire({
			title: "Studio Updated",
			showClass: {
				popup: "animated fadeInDown faster"
			},
			hideClass: {
				popup: "animated fadeOutUp faster"
			},
			html:
				'<a href="/studios" class="button is-success"> Update Successful </a>',
			showConfirmButton: false
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Heading className="has-text-centered">
						UpdateStudio
					</Heading>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Studio Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<h3>Studio Name</h3>
								<input
									id="name"
									type="text"
									onChange={ChangeHandler}
									value={name}
									className="input"
								/>
								<h3>Description</h3>
								<input
									id="description"
									type="text"
									onChange={ChangeHandler}
									value={description}
									className="input"
								/>
								<f3>Image</f3>
								<input
									id="image"
									type="text"
									onChange={ChangeHandler}
									value={image}
									className="input"
								/>
								<f3>Price</f3>
								<input
									id="price"
									type="Number"
									onChange={ChangeHandler}
									value={price}
									className="input"
								/>

								<br></br>
								<button
									type="submit"
									className="button is-success is-fullwidth"
								>
									Save Changes
								</button>
								<button
									onClick={close}
									type="button"
									className="button is-danger is-fullwidth"
								>
									Cancel
								</button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(updateStudioMutation, { name: "updateStudioMutation" }),
	graphql(getStudioQuery, {
		options: props => {
			return {
				variables: {
					id: props.selectedId
				}
			};
		},
		name: "getStudioQuery"
	})
)(UpdateStudio);
