import React, { useEffect, useState } from "react";
import logo from "./../logo.svg";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link, Redirect } from "react-router-dom";

// queries
import { getUsersQuery, getRolesQuery } from "../queries/queries.js";
import { createUserMutation } from "../queries/mutations.js";

const Register = function(props) {
	// Hooks
	const [id] = useState("");
	const [userName, setUserName] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [password, setPassword] = useState("");
	const [email, setEmail] = useState("");
	const [role, setRole] = useState("5de0cfca19e710429ef77010");

	function ChangeHandler(event) {
		if (event.target.id === "fName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lName") {
			setLastName(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "userName") {
			setUserName(event.target.value);
		} else if (event.target.id === "email") {
			setEmail(event.target.value);
		}
	}

	function clear() {
		setFirstName("");
		setLastName("");
		setPassword("");
		setUserName("");
		setEmail("");
	}

	const addUser = function(event) {
		event.preventDefault();
		console.log("ROLE: " + role);

		let emailTest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
			email
		);
		console.log(emailTest);

		if (
			userName === "" ||
			password === "" ||
			email === "" ||
			firstName === "" ||
			lastName === "" ||
			!emailTest
		) {
			Swal.fire({
				title: "Invalid or missing fields!",
				showClass: {
					popup: "animated flipInX"
				},
				hideClass: {
					popup: "animated fadeOutDown faster"
				}
			});
		} else {
			let newUser = {
				userName: userName,
				firstName: firstName,
				lastName: lastName,
				password: password,
				email: email,
				roleID: role
			};

			props.createUserMutation({
				variables: newUser,
				refetchQueries: [
					{
						query: getUsersQuery
					}
				]
			});
			Swal.fire({
				title: "Successfully Registred!",
				showClass: {
					popup: "animated flipInY"
				},
				hideClass: {
					popup: "animated fadeOutUp faster"
				}
			});
			props.close();
			clear();
		}
	};

	function getRole() {
		console.log(role);
		// console.log(role.id);
		// console.log(role.name);
		// console.log(props.getRolesQuery);
		// let roleData = props.getRolesQuery;
	}

	// const roleOptions = function() {
	// 	let roleData = props.getRolesQuery;
	// 	getRole();
	// 	if (roleData.loading) {
	// 		return <option>Loading.... </option>;
	// 	} else {
	// 		return roleData.getRoles.map(function(role) {
	// 			return (
	// 				<option value={role.id} key={role.id}>
	// 					{role.name}
	// 				</option>
	// 			);
	// 		});
	// 	}
	// };

	return (
		<Container>
			<br></br>
			<Columns className="has-text-centered ">
				<Columns.Column size={12}>
					<Card>
						<Card.Header>Register</Card.Header>
						<Card.Content>
							<form onSubmit={addUser}>
								<div className="field ">
									<label className="label" htmlFor="fname">
										First Name
									</label>
									<input
										id="fName"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={firstName}
									/>
								</div>
								<div className="field ">
									<label className="label" htmlFor="fname">
										UserName
									</label>
									<input
										id="userName"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={userName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="lname">
										Last Name
									</label>
									<input
										id="lName"
										className="input"
										type="text"
										onChange={ChangeHandler}
										value={lastName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="Password"
										onChange={ChangeHandler}
										value={password}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="email">
										Email
									</label>
									<input
										id="email"
										className="input"
										type="email"
										onChange={ChangeHandler}
										value={email}
									/>
								</div>

								<Button
									onClick={addUser}
									type="submit"
									color="success"
									fullwidth
									rounded
								>
									Add User
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(getRolesQuery, { name: "getRolesQuery" }),
	graphql(createUserMutation, { name: "createUserMutation" })
)(Register);
