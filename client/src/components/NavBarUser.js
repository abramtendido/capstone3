import React, { useState } from "react";
import { Navbar, Columns } from "react-bulma-components";
import { Link } from "react-router-dom";

const NavBarUser = function(props) {
	const [open, setOpen] = useState(false);
	let customLink = "";
	let role = localStorage.getItem("role");

	if (!props.userName) {
		customLink = (
			<Link to="/login" className="navbar-item">
				Login
			</Link>
		);
	} else {
		customLink = (
			<Link to="/logout" className="navbar-item">
				Logout
			</Link>
		);
	}
	return (
		<Navbar color="warning" active={open}>
			<Navbar.Brand>
				<Link to="/" className="navbar-item">
					<strong>
						<i class="fas fa-compact-disc"></i>
						Rentals
					</strong>
				</Link>
				<Navbar.Burger
					active={open}
					onClick={function() {
						setOpen(!open);
					}}
				/>
			</Navbar.Brand>

			<Navbar.Menu>
				{role === "user" ? (
					<Navbar.Container position="end">
						<Link className="navbar-item" to="/studios">
							Studios
						</Link>

						<Link className="navbar-item" to="/reservations">
							Reservations
						</Link>
						{customLink}
					</Navbar.Container>
				) : (
					<Navbar.Container position="end">
						{customLink}
					</Navbar.Container>
				)}
			</Navbar.Menu>
		</Navbar>
	);
};

//export the component
export default NavBarUser;
