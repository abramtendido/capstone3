import React, { useEffect, useState } from "react";
import logo from "./../logo.svg";
import {
	Heading,
	Container,
	Columns,
	Card,
	Button,
	Modal
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { DatePicker } from "antd";
import moment from "moment";

// queries
import {
	// getStatusesQuery,
	// getStatusQuery,
	// getStudiosQuery,
	// getUsersQuery,
	getStudioQuery
} from "../queries/queries.js";
import { createReservationMutation } from "../queries/mutations.js";

const CreateReservation = function(props) {
	const { RangePicker } = DatePicker;
	console.log(props);
	// Hooks
	// const [id, setId] = useState("");
	const [dateStart, setDateStart] = useState("");
	const [dateEnd, setDateEnd] = useState("");
	const [statusID, setStatusID] = useState("");
	const [studioID, setStudioID] = useState("");
	const [userID, setUserID] = useState("");

	let Studio = props.getStudioQuery.getStudio
		? props.getStudioQuery.getStudio
		: {};

	let userNameVal = localStorage.getItem("userName");

	if (!props.getStudioQuery.loading) {
		const setDefault = function() {
			setStatusID("5deedf537c9f6813807abe79");
			setUserID(localStorage.getItem("userID"));
			setStudioID(Studio.id);
		};

		if (userID === "") {
			setDefault();
		}
	}

	console.log("USERID: " + userID);
	console.log("STATUSID: " + statusID);
	console.log("STUDIOID: " + studioID);
	console.log(localStorage.getItem("userName"));

	function clear() {
		setDateStart("");
		setDateEnd("");
	}

	function ChangeHandler(args) {
		let startDate = args[0]._d;
		let endDate = args[1]._d;
		// setStatusID("5deedf537c9f6813807abe79");
		// setUserID(localStorage.getItem("userID"));
		// setStudioID(Studio.id);
		setDateStart(startDate);
		setDateEnd(endDate);
		console.log(args);
	}

	const addReservation = function(event) {
		event.preventDefault();
		if (dateStart === "" || dateEnd === "") {
			Swal.fire({
				title: "Invalid or missing fields!",
				showClass: {
					popup: "animated flipInX"
				},
				hideClass: {
					popup: "animated fadeOutDown faster"
				}
			});
		} else {
			let newReservation = {
				dateStart: dateStart,
				dateEnd: dateEnd,
				statusID: statusID,
				studioID: studioID,
				userID: userID
			};

			props.createReservationMutation({
				variables: newReservation
			});
			Swal.fire({
				title: "Team Updated",
				showClass: {
					popup: "animated fadeInDown faster"
				},
				hideClass: {
					popup: "animated fadeOutUp faster"
				},
				html:
					'<a href="/reservations" class="button is-success"> Reservation Made </a>',
				showConfirmButton: false
			});
		}
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Heading className="has-text-centered">Reserve</Heading>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Reservation Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addReservation}>
								<h3 className="has-text-centered has-text-weight-bold">
									Studio Name
								</h3>
								<p className="has-text-centered">
									{Studio.name}
								</p>

								<h3 className="has-text-centered has-text-weight-bold">
									User Name
								</h3>
								<p className="has-text-centered">
									{userNameVal}
								</p>

								<div className="has-text-centered">
									<RangePicker
										ranges={{
											Today: [moment(), moment()],
											"This Month": [
												moment().startOf("month"),
												moment().endOf("month")
											]
										}}
										onChange={ChangeHandler}
									/>
									<br />
								</div>

								<input type="hidden" value={userID} />

								<input type="hidden" value={statusID} />

								<input type="hidden" value={studioID} />

								<br></br>
								<button
									type="submit"
									className="button is-success is-fullwidth"
								>
									Submit Reservation
								</button>
								<Link to={"/studios"}>
									<button
										type="button"
										className="button is-danger is-fullwidth"
									>
										Cancel
									</button>
								</Link>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(createReservationMutation, { name: "createReservationMutation" }),
	// graphql(getUsersQuery, { name: "getUsersQuery" }),
	// graphql(getStatusesQuery, { name: "getStatusesQuery" }),
	// graphql(getStatusQuery, { name: "getStatusQuery" }),
	// graphql(getStudiosQuery, { name: "getStudiosQuery" }),
	graphql(getStudioQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getStudioQuery"
	})
)(CreateReservation);
