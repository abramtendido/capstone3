import React, { useState, useEffect } from "react";
import {
	Section,
	Heading,
	Container,
	Columns,
	Card,
	Button,
	Modal
} from "react-bulma-components";
import { Link, Redirect } from "react-router-dom";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import Swal from "sweetalert2";
import "../App.css";
import Register from "./Register";

// queries
// import { getMembersQuery } from "../queries/queries.js";
import { logInMutation } from "../queries/mutations.js";

const Login = function(props) {
	const [userName, setUserName] = useState("");
	const [password, setPassword] = useState("");
	const [logInSuccess, setLogInSuccess] = useState(false);
	const [showRegister, setShowRegister] = useState(false);

	function ChangeHandler(event) {
		if (event.target.id == "userName") {
			setUserName(event.target.value);
		} else setPassword(event.target.value);
	}

	function showRegisterHandler(event) {
		setShowRegister(true);
	}

	function closeRegisterHandler(event) {
		setShowRegister(false);
	}

	const formSubmitHandler = function(event) {
		event.preventDefault();
		let id = props.match.params.id;
		let updatedUser = {
			userName: userName,
			password: password
		};
		props
			.logInMutation({
				variables: updatedUser
			})
			.then(function(response) {
				console.log(response);

				let data = response.data.logInUser;
				console.log(data);

				if (data === null) {
					Swal.fire({
						position: "top-end",
						icon: "error",
						title: "Failed Wrong Credentials!",
						showConfirmButton: false,
						timer: 500
					});
				} else {
					localStorage.setItem("userName", data.userName);
					localStorage.setItem("role", data.role.name);
					localStorage.setItem("userID", data.id);
					setLogInSuccess(true);
					props.updateSession();
					Swal.fire({
						position: "top-end",
						icon: "success",
						title: "Logged in!",
						showConfirmButton: false,
						timer: 500
					});
				}
			});
	};

	if (!logInSuccess) {
		console.log("Login Failed!");
	} else {
		console.log("Login Success!");
		return <Redirect to="/users" />;
	}

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Modal show={showRegister} onClose={closeRegisterHandler}>
						<Modal.Content>
							<Register close={closeRegisterHandler} />
						</Modal.Content>
					</Modal>

					<br></br>
					<Card>
						<Card.Header>
							<Card.Header.Title>Login</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler} id="loginHolder">
								<label className="label">Username</label>
								<input
									id="userName"
									type="text"
									onChange={ChangeHandler}
									value={userName}
									placeholder="input username here"
									className="input"
								/>
								<label className="label">Password</label>
								<input
									id="password"
									type="Password"
									onChange={ChangeHandler}
									value={password}
									placeholder="input password here"
									className="input"
								/>
								<hr></hr>
								<button
									type="submit"
									className="button is-success is-fullwidth"
								>
									Login
								</button>
							</form>
						</Card.Content>
					</Card>
					<hr></hr>
					<div>
						<p className="text-center">
							{" "}
							Have no account? Register!{" "}
						</p>
						<button
							onClick={showRegisterHandler}
							className="button is-info is-fullwidth"
						>
							Register
						</button>
					</div>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(graphql(logInMutation, { name: "logInMutation" }))(
	Login
);
