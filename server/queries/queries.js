// Dependencies
const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const bcrypt = require("bcrypt");
const moment = require("moment");
const uuid = require("uuid/v1");
const fs = require("fs");

// Models
const user = require("../models/user");
const role = require("../models/role");
const studio = require("../models/studio");
const reservation = require("../models/reservation");
const status = require("../models/status");

// CRUD

// Resolver for Date Schema to be able to handle date data types
const customScalarResolver = {
	Date: GraphQLDateTime
};

// Schema
const typeDefs = gql`
	# Get requests
	scalar Date

	type RoleType {
		id: ID
		name: String
	}

	type UserType {
		id: ID
		userName: String
		firstName: String
		lastName: String
		password: String
		email: String
		roleID: String
		role: RoleType
	}

	type StudioType {
		id: ID
		name: String
		description: String
		image: String
		price: Float
	}

	type StatusType {
		id: ID
		name: String
	}

	type ReservationType {
		id: ID
		dateStart: Date
		dateEnd: Date
		statusID: String
		status: StatusType
		studioID: String
		studio: StudioType
		userID: String
		user: UserType
	}

	type Query {
		getRoles: [RoleType]
		getRole(id: ID!): RoleType
		getUsers: [UserType]
		getUser(id: ID!): UserType
		getStudios: [StudioType]
		getStudio(id: ID!): StudioType
		getStatuses: [StatusType]
		getStatus(id: ID!): StatusType
		getReservations: [ReservationType]
		getReservation(id: ID!): ReservationType
	}

	# Create, Update, Delete

	type Mutation {
		# Create
		createRole(name: String!): RoleType

		createStatus(name: String!): StatusType

		createUser(
			userName: String!
			firstName: String!
			lastName: String!
			password: String!
			email: String!
			roleID: String!
		): UserType

		createStudio(
			name: String!
			description: String!
			image: String!
			price: Float!
		): StudioType

		createReservation(
			dateStart: Date!
			dateEnd: Date!
			studioID: String!
			statusID: String!
			userID: String!
		): ReservationType

		# End of Create

		#Update

		updateRole(name: String!): RoleType

		updateStatus(name: String!): StatusType

		updateUser(
			id: ID!
			userName: String!
			firstName: String!
			lastName: String!
			password: String!
			email: String!
			roleID: String!
		): UserType

		updateStudio(
			id: ID!
			name: String!
			description: String!
			image: String!
			price: Float!
		): StudioType

		updateReservation(id: ID!, statusID: String!): ReservationType

		#End of Update

		#Delete

		deleteRole(id: ID!): Boolean

		deleteStatus(id: ID!): Boolean

		deleteUser(id: ID!): Boolean

		deleteStudio(id: ID!): Boolean

		deleteReservation(id: ID!): Boolean

		#End of Delete

		logInUser(userName: String!, password: String!): UserType
	}
`;
// End of Schema

// Resolver
const resolvers = {
	Query: {
		// Get All

		getRoles: function() {
			return role.find({});
		},

		getStatuses: function() {
			return status.find({});
		},

		getUsers: function() {
			return user.find({});
		},

		getStudios: function() {
			return studio.find({});
		},

		getReservations: function() {
			return reservation.find({});
		},

		// End of Get All

		// Get Specific
		getRole: function(parent, args) {
			return role.findById(args.id);
		},

		getStatus: function(parent, args) {
			return status.findById(args.id);
		},

		getUser: function(parent, args) {
			return user.findById(args.id);
		},

		getStudio: function(parent, args) {
			return studio.findById(args.id);
		},

		getReservation: function(parent, args) {
			return reservation.findById(args.id);
		}
		// End of Get Specific
	},

	Mutation: {
		// CREATE
		createRole: function(parent, args) {
			let newRole = role({
				name: args.name
			});

			return newRole.save();
		},

		createStatus: function(parent, args) {
			let newStatus = status({
				name: args.name
			});

			return newStatus.save();
		},

		createUser: function(parent, args) {
			console.log(args);

			let newUser = user({
				userName: args.userName,
				firstName: args.firstName,
				lastName: args.lastName,
				password: bcrypt.hashSync(args.password, 10),
				email: args.email,
				roleID: args.roleID
			});

			return newUser.save();
		},

		createStudio: function(parent, args) {
			let imageString = args.image;

			let imageBase = imageString.split(";base64,").pop();
			console.log(imageBase);

			let imageLocation = "images/" + uuid() + ".png";
			console.log(imageLocation);

			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);

			let newStudio = studio({
				name: args.name,
				description: args.description,
				image: imageLocation,
				price: args.price
			});

			return newStudio.save();
		},

		createReservation: function(parent, args) {
			let newReservation = reservation({
				dateStart: args.dateStart,
				dateEnd: args.dateEnd,
				statusID: args.statusID,
				studioID: args.studioID,
				userID: args.userID
			});

			return newReservation.save();
		},

		// END OF CREATE

		// UPDATE
		updateRole: function(parent, args) {
			return role.findOneAndUpdate(
				{ _id: args.id },
				{ $set: { name: args.name } },
				function() {
					console.log("role updated");
				}
			);
		},

		updateStatus: function(parent, args) {
			return status.findOneAndUpdate(
				{ _id: args.id },
				{ $set: { name: args.name } },
				function() {
					console.log("status updated");
				}
			);
		},

		updateUser: function(parent, args) {
			return user.findOneAndUpdate(
				{ _id: args.id },
				{
					$set: {
						userName: args.userName,
						firstName: args.firstName,
						lastName: args.lastName,
						password: args.password,
						roleID: args.roleID
					}
				},
				function() {
					console.log("user updated");
				}
			);
		},

		updateStudio: function(parent, args) {
			return studio.findOneAndUpdate(
				{ _id: args.id },
				{
					$set: {
						name: args.name,
						description: args.description,
						image: args.image,
						price: args.price
					}
				},
				function() {
					console.log("studio updated");
				}
			);
		},

		updateReservation: function(parent, args) {
			return reservation.findOneAndUpdate(
				{ _id: args.id },
				{
					$set: {
						statusID: args.statusID
					}
				},
				function() {
					console.log("reservation updated");
				}
			);
		},
		// END OF UPDATE

		// DELETE
		deleteRole: function(parent, args) {
			let condition = args.id;

			return role.findByIdAndDelete(condition).then(function(user, err) {
				if (err) {
					return false;
				} else {
					return true;
				}
				console.log(err);
			});
		},

		deleteStatus: function(parent, args) {
			let condition = args.id;

			return status
				.findByIdAndDelete(condition)
				.then(function(user, err) {
					if (err) {
						return false;
					} else {
						return true;
					}
					console.log(err);
				});
		},

		deleteUser: function(parent, args) {
			let condition = args.id;

			return user.findByIdAndDelete(condition).then(function(user, err) {
				if (err) {
					return false;
				} else {
					return true;
				}
				console.log(err);
			});
		},

		deleteStudio: function(parent, args) {
			let condition = args.id;

			return studio
				.findByIdAndDelete(condition)
				.then(function(studio, err) {
					if (err) {
						return false;
					} else {
						return true;
					}
					console.log(err);
				});
		},

		deleteReservation: function(parent, args) {
			let condition = args.id;

			return reservation
				.findByIdAndDelete(condition)
				.then(function(reservation, err) {
					if (err) {
						return false;
					} else {
						return true;
					}
					console.log(err);
				});
		},

		// END OF DELETE

		// LOGIN
		logInUser: function(parent, args) {
			console.log("Logging in...");
			return user
				.findOne({ userName: args.userName })
				.then(function(user) {
					if (user === null) {
						return null;
					}

					let hashedPassword = bcrypt.compareSync(
						args.password,
						user.password
					);

					if (!hashedPassword) {
						return null;
					} else {
						return user;
					}
				});
		}
		// END OF LOGIN
	},

	UserType: {
		role: function(parent, args) {
			return role.findById(parent.roleID);
		}
	},

	ReservationType: {
		user: (parent, args) => {
			return user.findById(parent.userID);
		},
		studio: (parent, args) => {
			return studio.findById(parent.studioID);
		},
		status: (parent, args) => {
			return status.findById(parent.statusID);
		}
	}
};

// Create instance of apollo server
const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
