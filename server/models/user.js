// Dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
	{
		userName: { type: String, required: true, unique: true },
		firstName: { type: String, required: true },
		lastName: { type: String, required: true },
		password: { type: String, required: true },
		email: { type: String, required: true },
		roleID: { type: String }
	},
	{
		timestamps: true
	}
);

//export the model as a module
module.exports = mongoose.model("user", userSchema);
