// Dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const statusSchema = new Schema(
	{
		name: { type: String, required: true }
	},
	{
		timestamps: true
	}
);

//export the model as a module
module.exports = mongoose.model("status", statusSchema);
