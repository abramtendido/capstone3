// Dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reservationSchema = new Schema(
	{
		dateStart: { type: Date, required: true },
		dateEnd: { type: Date, required: true },
		statusID: { type: String, required: true },
		studioID: { type: String, required: true },
		userID: { type: String, required: true }
	},
	{
		timestamps: true
	}
);

//export the model as a module
module.exports = mongoose.model("reservation", reservationSchema);
