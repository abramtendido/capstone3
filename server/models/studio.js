// Dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const studioSchema = new Schema(
	{
		name: { type: String, required: true },
		description: { type: String, required: true },
		image: { type: String },
		price: { type: Number, required: true }
	},
	{
		timestamps: true
	}
);

//export the model as a module
module.exports = mongoose.model("studio", studioSchema);
