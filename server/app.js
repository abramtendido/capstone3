// declare dependencies
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");

//initialize express app
const app = express();
// const port = 4000;

// Database connection
// local
// mongoose.connect("mongodb://localhost:27017/merng_tracker", { useNewUrlParser:true })
// online

let databaseURL =
	process.env.DATABASE_URL ||
	"mongodb+srv://admin:admin@singapoor-c40e8.mongodb.net/capstone3?retryWrites=true&w=majority";

mongoose.connect(databaseURL, {
	useCreateIndex: true,
	useNewUrlParser: true
});

// check if connection succeeded
mongoose.connection.once("open", () => {
	console.log("connected to mongodb server!");
});

// increase the limit of uploaded files
app.use(bodyParser.json({ limit: "15mb" }));

// allow users to access a folder in the server by serving the static data
app.use("/images", express.static("images"));
app.use(cors());

// import the instantiation
const server = require("./queries/queries.js");

//the app wil be served by the apollo serve instead of express
server.applyMiddleware({
	app,
	path: "/capstone3"
});

let port = process.env.PORT || 4000;

// server initialization
app.listen(port, () => {
	console.log(
		`🚀  Server ready at http://localhost:4000${server.graphqlPath}`
	);
});
